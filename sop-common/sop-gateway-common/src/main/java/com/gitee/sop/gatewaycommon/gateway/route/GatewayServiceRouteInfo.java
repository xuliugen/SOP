package com.gitee.sop.gatewaycommon.gateway.route;

import com.gitee.sop.gatewaycommon.bean.BaseServiceRouteInfo;
import lombok.Data;

import java.util.List;

/**
 * @author thc
 */
public class GatewayServiceRouteInfo extends BaseServiceRouteInfo<GatewayRouteDefinition> {
}