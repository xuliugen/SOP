package com.gitee.sop.gateway.mapper;

import com.gitee.fastmybatis.core.mapper.CrudMapper;
import com.gitee.sop.gateway.entity.IsvInfo;


/**
 * @author tanghc
 */
public interface IsvInfoMapper extends CrudMapper<IsvInfo, Long> {
}
