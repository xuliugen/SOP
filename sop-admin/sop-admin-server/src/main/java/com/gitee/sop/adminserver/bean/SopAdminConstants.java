package com.gitee.sop.adminserver.bean;

/**
 * @author tanghc
 */
public class SopAdminConstants {
    /**
     * zookeeper存放接口路由信息的根目录
     */
    public static final String SOP_SERVICE_ROUTE_PATH = "/com.gitee.sop.route";

    /**
     * zookeeper存放路由授权信息根目录
     */
    public static final String SOP_ROUTE_PERMISSION_PATH = "/com.gitee.sop.isv.route.permission";

    /**
     * 消息监听路径
     */
    public static final String SOP_MSG_CHANNEL_PATH = "/com.gitee.sop.channel";

}
