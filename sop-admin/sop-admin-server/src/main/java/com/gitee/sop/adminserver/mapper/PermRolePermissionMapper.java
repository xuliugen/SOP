package com.gitee.sop.adminserver.mapper;

import com.gitee.fastmybatis.core.mapper.CrudMapper;

import com.gitee.sop.adminserver.entity.PermRolePermission;


/**
 * @author tanghc
 */
public interface PermRolePermissionMapper extends CrudMapper<PermRolePermission, Long> {
}
